//
//  MeasurementsListViewController.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import "MeasurementsListViewController.h"
#import "MeasurementsDatabase.h"
#import "MeasurementCellView.h"

@interface MeasurementsListViewController ()

@end

@implementation MeasurementsListViewController {
    NSArray *_measurements;
}

@synthesize measurements = _measurements;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"View Measurements";
        self.tabBarItem.image = [UIImage imageNamed:@"view"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _measurements = [MeasurementsDatabase getMeasurements];
    self.title = @"Measurements";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_measurements count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    MeasurementCellView *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MeasurementCellView" owner:self options:NULL];
        cell = (MeasurementCellView*)[nib objectAtIndex:0];
        cell.owner = self;
    }
    
    Measurement *measurement = [_measurements objectAtIndex:indexPath.row];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    [dateFormatter setDateFormat:MEASUREMENTDATEFORMAT];
    cell.date.text = [dateFormatter stringFromDate:measurement.date];
    cell.weight.text = [NSString stringWithFormat:@"%3.1f", measurement.weight];
    cell.sugar.text = [NSString stringWithFormat:@"%3.0f", measurement.sugar];
    cell.pressure.text = [NSString stringWithFormat:@"%@", [Measurement stringFromPressure:measurement.pressure]];
    cell.heartRate.text = [NSString stringWithFormat:@"%d", measurement.heartRate];  
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(void)reloadData {
    _measurements = [MeasurementsDatabase getMeasurements];
}

-(void)viewWillAppear:(BOOL)animated {
    static int length = 0;
    [super viewWillAppear:animated];
    [self reloadData];
    if(length != _measurements.count) {
        [self.tableView reloadData];
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: _measurements.count - 1 inSection: 0];
        [self.tableView scrollToRowAtIndexPath:ipath
                              atScrollPosition:UITableViewScrollPositionTop 
                                      animated:YES];
        length = _measurements.count;
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

-(void)dealloc {
    _measurements = nil;
}

@end
