//
//  MeasurementsListViewController.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeasurementsListViewController : UITableViewController {
}

@property (nonatomic, strong) NSArray *measurements;

-(void)reloadData;

@end
