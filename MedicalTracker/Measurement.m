//
//  Measurement.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import "Measurement.h"


@implementation Measurement

@synthesize date, weight, heartRate, pressure, sugar;

+(Pressure)pressureFromUTF8String:(const char *)string {
    char buffer[50];
    Pressure output;
    char *token;
    char *search = "-";
    strcpy(buffer, string);
    token = strtok(buffer, search);
    output.systolic = atoi(token); // zmienic na strtol(str, (char **)NULL, 10)
    token = strtok(NULL, search);
    output.diastolic = atoi(token);
    return output;
}

+(NSString *)stringFromPressure:(Pressure)pressure {
    NSString* systolic = [NSString stringWithFormat:@"%d", pressure.systolic];
    NSString* diastolic = [NSString stringWithFormat:@"%d", pressure.diastolic];
    return [NSString stringWithFormat:@"%@-%@", systolic, diastolic];
}

@end
