//
//  AppDelegate.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 07.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
