//
//  MeasurementsDatabase.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "/usr/include/sqlite3.h"
#import "Measurement.h"

@interface MeasurementsDatabase : NSObject {
}

+(NSArray*)getMeasurements;
+(NSArray*)getMeasurementsFromDate:(NSDate*)date;
+(void)deleteMeasurementWithDate:(NSDate*)date;
+(void)deleteMeasurementWithDateString:(NSString*)dateString;
+(BOOL)addMeasurement:(Measurement*)measurement;

@end
