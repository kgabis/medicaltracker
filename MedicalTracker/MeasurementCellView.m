//
//  MeasurementCell.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 25.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import "MeasurementCellView.h"
#import "MeasurementsDatabase.h"

@implementation MeasurementCellView

@synthesize date, weight, sugar, heartRate, pressure, owner;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)deleteMeasurement:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle: @"Deleting" 
                          message:@"Are you sure?"
                          delegate:self
                          cancelButtonTitle:@"No"
                          otherButtonTitles:@"Yes",
                          nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		//No
	}
	else if (buttonIndex == 1)
	{
        //Yes
        [MeasurementsDatabase deleteMeasurementWithDateString:date.text];
        [owner viewWillAppear:YES];
	}
}

@end
