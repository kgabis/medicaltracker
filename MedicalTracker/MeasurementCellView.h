//
//  MeasurementCell.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 25.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeasurementCellView : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *date;
@property (nonatomic, strong) IBOutlet UILabel *weight;
@property (nonatomic, strong) IBOutlet UILabel *sugar;
@property (nonatomic, strong) IBOutlet UILabel *pressure;
@property (nonatomic, strong) IBOutlet UILabel *heartRate;
@property (nonatomic, weak) UITableViewController *owner;

-(IBAction)deleteMeasurement:(id)sender;

@end
