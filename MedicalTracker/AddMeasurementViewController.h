//
//  FirstViewController.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 07.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeasurementsDatabase.h"

@interface AddMeasurementViewController : UIViewController

@property IBOutlet UITextField *weight;
@property IBOutlet UITextField *sugar;
@property IBOutlet UITextField *pressureSystolic;
@property IBOutlet UITextField *pressureDiastolic;
@property IBOutlet UITextField *heartRate;

-(IBAction)addMeasurement:(id)sender;
-(IBAction)valueChanged:(UITextField*)sender;
-(IBAction)backgroundTouched:(id)sender;
@end
