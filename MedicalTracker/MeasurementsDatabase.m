//
//  MeasurementsDatabase.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import "MeasurementsDatabase.h"

@implementation MeasurementsDatabase {
}

static sqlite3 *_database;
static NSString *_databasePath;

+(void)initializeDatabase {
    NSString *docsDir;
    NSArray *dirPaths;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    _databasePath = [[NSString alloc] initWithString: 
                    [docsDir stringByAppendingPathComponent:@"measurements.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath:_databasePath] == NO) {
        const char *dbpath = [_databasePath UTF8String];
        if (sqlite3_open(dbpath, &_database) == SQLITE_OK) {
            char *errMsg;
            const char *sql_stmt =
            "CREATE TABLE IF NOT EXISTS measurements (id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, weight FLOAT, sugar FLOAT, pressure TEXT, heartRate INTEGER)";
            
            if (sqlite3_exec(_database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
//                NSLog(@"%@\n",  @"Failed to create table");
            }
            sqlite3_close(_database);
        }
        else {
//            NSLog(@"%@\n", @"Failed to open/create database");
        }
        
    }
}

+(NSArray *)getMeasurements {
    if (!_databasePath) {
        [MeasurementsDatabase initializeDatabase];
    }
    NSMutableArray* output = [[NSMutableArray alloc] init];
    
    const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_database) == SQLITE_OK)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
        [dateFormatter setDateFormat:MEASUREMENTDATEFORMAT];
        NSString *dateString;
        NSString *querySQL = 
            @"SELECT date, weight, sugar, pressure, heartRate FROM measurements";
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                Measurement *newMeasurement = [[Measurement alloc] init];
                dateString = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
                newMeasurement.date = [dateFormatter dateFromString:dateString];
                newMeasurement.weight = sqlite3_column_double(statement, 1);
                newMeasurement.sugar = sqlite3_column_double(statement, 2);
                newMeasurement.pressure = [Measurement pressureFromUTF8String: (const char*)sqlite3_column_text(statement, 3)];
                newMeasurement.heartRate = sqlite3_column_int(statement, 4);
                [output addObject:newMeasurement];
                
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(_database);
    }    
    return [NSArray arrayWithArray:output];
}

+(NSArray*)getMeasurementsFromDate:(NSDate*)date {
    if (!_databasePath) {
        [MeasurementsDatabase initializeDatabase];
    }
    NSMutableArray* output = [[NSMutableArray alloc] init];
    
    const char *dbpath = [_databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_database) == SQLITE_OK)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
        [dateFormatter setDateFormat:MEASUREMENTDATEFORMAT];
        NSString *dateString = [dateFormatter stringFromDate:date];
        NSString *querySQL = [NSString stringWithFormat: 
        @"SELECT date, weight, sugar, pressure, heartRate FROM measurements WHERE date=\"%@\"", 
                                    dateString];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                Measurement *newMeasurement = [[Measurement alloc] init];
                newMeasurement.date = date;
                newMeasurement.weight = sqlite3_column_double(statement, 1);
                newMeasurement.sugar = sqlite3_column_double(statement, 2);
                newMeasurement.pressure = [Measurement pressureFromUTF8String: (const char*)sqlite3_column_text(statement, 3)];
                newMeasurement.heartRate = sqlite3_column_int(statement, 4);
                [output addObject:newMeasurement];
                
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(_database);
    }    
    return [NSArray arrayWithArray:output];
}

+(void)deleteMeasurementWithDate:(NSDate*)date {
    if (!_databasePath) {
        [MeasurementsDatabase initializeDatabase];
    }
    
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_database) == SQLITE_OK) {
        char *errMsg;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
        [dateFormatter setDateFormat:MEASUREMENTDATEFORMAT];
        NSString *dateString = [dateFormatter stringFromDate:date];
        NSString *querySQL = [NSString stringWithFormat: 
                                @"delete from measurements where date = \"%@\"", dateString];
        const char *sql_stmt = [querySQL UTF8String];
        if (sqlite3_exec(_database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
//            NSLog(@"%@\n",  @"Failed to delete");
        }
        sqlite3_close(_database);
    }
    else {
//        NSLog(@"%@\n", @"Failed to open database");
    }    
}

+(void)deleteMeasurementWithDateString:(NSString *)dateString {
    if (!_databasePath) {
        [MeasurementsDatabase initializeDatabase];
    }
    
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_database) == SQLITE_OK) {
        char *errMsg;
        NSString *querySQL = [NSString stringWithFormat: 
                              @"delete from measurements where date = \"%@\"", dateString];
        const char *sql_stmt = [querySQL UTF8String];
        if (sqlite3_exec(_database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
//            NSLog(@"%@\n",  @"Failed to delete");
        }
        sqlite3_close(_database);
    }
    else {
//        NSLog(@"%@\n", @"Failed to open database");
    }    
}

+(BOOL)addMeasurement:(Measurement*)measurement {
    BOOL result = NO;
    if (!_databasePath) {
        [MeasurementsDatabase initializeDatabase];
    }
    sqlite3_stmt    *statement;
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_database) == SQLITE_OK)
    {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
        [dateFormatter setDateFormat:MEASUREMENTDATEFORMAT];
        NSString *dateString = [dateFormatter stringFromDate:currentDate];
        NSString *pressureString = [Measurement stringFromPressure:measurement.pressure];
        
        NSString *insertSQL = [NSString stringWithFormat: 
        @"INSERT INTO measurements (date, weight, sugar, pressure, heartRate) VALUES (\"%@\", %f, %f,\"%@\", %d)", 
                               dateString, 
                               measurement.weight, 
                               measurement.sugar, 
                               pressureString, 
                               measurement.heartRate];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(_database, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
//            NSLog(@"%@\n", @"Measurement added");
            result = YES;
        } else {
//            NSLog(@"%@\n", @"Failed to add measurement");
            result = NO;
        }
        sqlite3_finalize(statement);
        sqlite3_close(_database);
    }
    else {
        result = NO;
    }
    
    return result;
}

@end
