//
//  main.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 07.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
