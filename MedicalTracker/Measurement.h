//
//  Measurement.h
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 24.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MEASUREMENTDATEFORMAT @"dd.MM.yyyy HH:mm:ss"

typedef struct {
    int systolic;
    int diastolic;
} Pressure;

@interface Measurement : NSObject {
}

@property NSDate* date;
@property float weight;
@property float sugar;
@property Pressure pressure;
@property int heartRate;

+(Pressure)pressureFromUTF8String:(const char*)string; //format: XXX-XXX
+(NSString*)stringFromPressure:(Pressure)pressure;
@end
