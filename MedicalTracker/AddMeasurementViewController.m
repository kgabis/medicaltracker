//
//  FirstViewController.m
//  MedicalTracker
//
//  Created by Krzysztof Gabis on 07.06.2012.
//  Copyright (c) 2012 Krzysztof Gabis. All rights reserved.
//

#import "AddMeasurementViewController.h"

@interface AddMeasurementViewController ()

@end

@implementation AddMeasurementViewController {
}

@synthesize weight, sugar, pressureSystolic, pressureDiastolic, heartRate;

-(IBAction)addMeasurement:(id)sender {
    Measurement* measurement = [[Measurement alloc] init];
    UIAlertView *message;
    Pressure pressure;
    measurement.weight = self.weight.text.floatValue;
    measurement.sugar = self.sugar.text.floatValue;
    pressure.systolic = self.pressureSystolic.text.intValue;
    pressure.diastolic = self.pressureDiastolic.text.intValue;
    measurement.pressure = pressure;
    measurement.heartRate = self.heartRate.text.intValue;

    if([MeasurementsDatabase addMeasurement:measurement]) {
        message = [[UIAlertView alloc] initWithTitle:@"Success!"
                                             message:@"Measurement successfully added."
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil];
        self.weight.text = @"";
        self.sugar.text = @"";
        self.pressureSystolic.text = @"";
        self.pressureDiastolic.text = @"";
        self.heartRate.text = @"";
        
    }
    else {
        message = [[UIAlertView alloc] initWithTitle:@"Error!"
                                            message:@"Measurement not added."
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
    }
    [message show];
    [self.view endEditing:YES];
}

-(void)valueChanged:(UITextField *)sender {
    if (![sender.text isEqualToString:@""] && sender.text.floatValue == 0.0f) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error!"
                                             message:@"Invalid data."
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil];
        [message show];
        sender.text = @"";
    }
    [self.view endEditing:YES];
}

-(void)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Add Measurement";
        self.tabBarItem.image = [UIImage imageNamed:@"add"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
